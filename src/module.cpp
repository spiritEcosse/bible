#include "module.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTextDocumentFragment>
#include <QRegularExpression>
#include <QDebug>
#include <qmath.h>

#define REGISTRY_FILE "/usr/share/bible/registry/registry.json"

Module::Module(
        const QString &name,
        const QString &description,
        const QString &abbreviation,
        const QString &information,
        const QLocale &language,
        const QString &language_load,
        const QDate &update,
        const QList<QString> &urls,
        const QList<QHash<QString, QString>> &locale_desc,
        const QString &comment,
        const quint32 &size,
        const QString &region,
        const bool &def,
        const bool &hidden,
        const QString &copyright
        )
    : m_name(name), m_description(description), m_abbreviation(abbreviation),
      m_information(information), m_language(language), m_language_load(language_load),
      m_update(update), m_urls(urls), m_locale_desc(locale_desc), m_comment(comment),
      m_size(size), m_region(region), m_def(def), m_hidden(hidden), m_copyright(copyright)
{}

QDate Module::update() const
{
    return m_update;
}

QString Module::description() const
{
    return m_description;
}

QString Module::abbreviation() const
{
    return m_abbreviation;
}

quint32 Module::size() const
{
    return m_size;
}

QLocale Module::language() const
{
    return m_language;
}

QString Module::information() const
{
    return m_information;
}

QString Module::comment() const
{
    return m_comment;
}

QString Module::section() const
{
    return m_language.language() == QLocale::C
            ? "Plans"
            : m_language.languageToString(m_language.language());
}

QLocale ModuleModel::correctLanguage(const QJsonObject &jsonObject) const
{
    return jsonObject.value("lng").toString();
}

quint32 ModuleModel::correctSize(const QJsonValue &jsonValue) const
{
    QRegularExpression re("^([+-]?\\d*\\.?\\d+)(\\w{1})$", QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatch match = re.match(jsonValue.toString());
    double size = 0;
    QStringList dimensions = {"K", "M", "G"};

    if (match.hasMatch()) {
        size = match.captured(1).toDouble();
        QString dimension = match.captured(2).toUpper();
        size *= qPow(1024, dimensions.indexOf(dimension) + 1);
    }
//ToDo replace on formattedDataSize
    return size;
}

ModuleModel::ModuleModel(QObject *parent) : QAbstractListModel(parent)
{
    QFile file(REGISTRY_FILE);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return ;

    QByteArray data = file.readAll();
    file.close();
    QJsonDocument document = QJsonDocument::fromJson(data);
    QJsonArray downloads = document.object().value("downloads").toArray();

    foreach(const QJsonValue & jsonValue, downloads)
        m_modules.append(this->parserJson(jsonValue));
}

int ModuleModel::rowCount(const QModelIndex & /* parent */) const{
    return m_modules.count();
}

Module ModuleModel::parserJson(const QJsonValue &jsonValue) const
{
    QList<QString> urls;
    QJsonObject jsonObject = jsonValue.toObject();
    foreach(const QJsonValue & url, jsonObject.value("url").toArray())
        urls.append(url.toString());

    QList<QHash<QString, QString>> locale_desc;
    foreach(const QJsonValue & val, jsonObject.value("lds").toArray()) {
        QHash<QString, QString> hash;
        hash["lng"] = val.toObject().value("lng").toString();
        hash["des"] = val.toObject().value("des").toString();
        locale_desc.append(hash);
    }

    return Module(
                jsonObject.value("fil").toString(),
                jsonObject.value("des").toString(),
                jsonObject.value("abr").toString(),
                QTextDocumentFragment::fromHtml(
                    jsonObject.value("inf").toString()
                    ).toPlainText(),
                this->correctLanguage(jsonObject),
                jsonObject.value("aln").toString(),
                QDate::fromString(
                    jsonObject.value("upd").toString(), "yyyy-MM-dd"
                ),
                urls,
                locale_desc,
                jsonObject.value("cmt").toString(),
                this->correctSize(jsonObject.value("siz")),
                jsonObject.value("region").toString(),
                jsonObject.value("def").toBool(),
                jsonObject.value("xxxhid").toBool(),
                jsonObject.value("lic").toString()
                );
}


QVariant ModuleModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_modules.count())
        return QVariant();

    const Module &module = m_modules[index.row()];

    if (role == UpdateRole)
        return module.update();
    if (role == DescriptionRole)
        return module.description();
    if (role == LanguageRole)
        return module.language();
    if (role == InformationRole)
        return module.information();
    if (role == CommentRole)
        return module.comment();
    if (role == AbbreviationRole)
        return module.abbreviation();
    if (role == SizeRole)
        return module.size();
    if (role == SectionRole)
        return module.section();

    return QVariant();
}


QHash<int, QByteArray> ModuleModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[UpdateRole] = "updateDate";
    roles[DescriptionRole] = "description";
    roles[AbbreviationRole] = "abbreviation";
    roles[InformationRole] = "information";
    roles[CommentRole] = "comment";
    roles[SizeRole] = "size";
    roles[LanguageRole] = "language";
    roles[SectionRole] = "section";
    return roles;
}

ModuleModel::~ModuleModel()
{

}

ModuleProxyModel::~ModuleProxyModel()
{
}

ModuleProxyModel::ModuleProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include "chapter.h"
#include "dbmanager.h"
#include "versesmodel.h"
#include "history.h"
#include "module.h"

int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/bible.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
    //
    // To display the view, call "show()" (will show fullscreen on device).

    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
    QScopedPointer<QQuickView> view(SailfishApp::createView());

    QQmlContext *ctxt = view.data()->rootContext();
    ChaptersModel chaptersListModel;
    ctxt->setContextProperty("chaptersListModel", &chaptersListModel);

    VersesModel verses;
    ctxt->setContextProperty("verses", &verses);

    FilterProxyModel filterVerses;
    filterVerses.setSourceModel(&verses);
    filterVerses.setFilterRole(VersesModel::ContentRole);
    ctxt->setContextProperty("filterVerses", &filterVerses);

    ModuleProxyModel moduleProxyModel;
    ModuleModel moduleModel;
    moduleProxyModel.setSourceModel(&moduleModel);
    moduleProxyModel.setFilterRole(ModuleModel::DescriptionRole);
    moduleProxyModel.setSortRole(ModuleModel::SectionRole);
    moduleProxyModel.setFilterCaseSensitivity(Qt::CaseInsensitive);
    moduleProxyModel.setSortCaseSensitivity(Qt::CaseInsensitive);
    moduleProxyModel.setDynamicSortFilter(true);
    moduleProxyModel.sort(0, Qt::AscendingOrder);
    ctxt->setContextProperty("moduleProxyModel", &moduleProxyModel);

    HistoryModel stories;
    ctxt->setContextProperty("stories", &stories);

    view->setSource(SailfishApp::pathToMainQml());
    view->show();
    return app->exec();
}

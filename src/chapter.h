#ifndef CHAPTER_H
#define CHAPTER_H

#include <QAbstractListModel>
#include "booksmodel.h"

class Chapter
{
public:
    Chapter(const quint16 &verse_id, const quint8 &chapter, const Book &book);

    quint16 verse_id() const;
    quint8 chapter() const;
    quint8 book_id() const;
    quint16 book_chapters() const;
    Book book() const;
    bool visible() const;
    QString section() const;
private:
    quint16 m_verse_id;
    quint8 m_chapter;
    Book m_book;
    bool m_visible;
};

class ChaptersModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum ChaptersRoles {
        VerseIdRole,
        ChapterRole,
        SectionRole,
        VisibleRole,
        BookChaptersRole,
        BookIdRole
    };

    ChaptersModel(QObject *parent = 0);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    void addChapter(const Chapter &chapter);
protected:
    QHash<int, QByteArray> roleNames() const;
private:
//    DbManager* db;
    QList<Chapter> m_chapters;
};

#endif // CHAPTER_H

#ifndef VERSESMODEL_H
#define VERSESMODEL_H

#include <QAbstractListModel>
#include "dbmanager.h"
#include "booksmodel.h"
#include <QSortFilterProxyModel>

class Verse
{
public:
    Verse(
            const quint16 &id,
            const quint8 &chapter,
            const quint16 &verse,
            const QString &content,
            const Book &book
    );

    quint16 id() const;
    quint8 chapter() const;
    quint8 book_id() const;
    quint16 verse() const;
    QString content() const;
    QString section() const;
    Book book() const;
private:
    quint16 m_id;
    quint8 m_chapter;
    quint16 m_verse;
    QString m_content;
    QString m_section;
    Book m_book;
};

class VersesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum VerseRoles {
        IdRole,
        BookRole,
        ChapterRole,
        VerseRole,
        ContentRole,
        SectionRole,
    };

    VersesModel(QObject *parent = 0);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    void addVerse(const Verse &verse);

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    DbManager* db;
    QList<Verse> m_verses;
};

class FilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:

    FilterProxyModel(QObject* parent = 0);

    ~FilterProxyModel();

    Q_INVOKABLE void setFilterString(QString string);

    void setSortOrder(bool checked);
};

#endif // VERSESMODEL_H

#include "versesmodel.h"
#include <QSqlQuery>

Verse::Verse(
        const quint16 &id,
        const quint8 &chapter,
        const quint16 &verse,
        const QString &content,
        const Book &book
        )
    : m_id(id), m_chapter(chapter), m_verse(verse), m_content(content), m_book(book)
{
}

quint16 Verse::id() const
{
    return m_id;
}

Book Verse::book() const
{
    return m_book;
}

quint8 Verse::chapter() const
{
    return m_chapter;
}

quint16 Verse::verse() const
{
    return m_verse;
}

QString Verse::content() const
{
    return m_content;
}

QString Verse::section() const
{
    return book().title() + "  ch. " + QString::number(chapter());
}

quint8 Verse::book_id() const
{
    return book().id();
}

VersesModel::VersesModel(QObject *parent) : QAbstractListModel(parent)
{
    QSqlQuery query(DbManager::getInstance()->db);
    query.exec("SELECT verses.id, chapter, verse, content, "
               "books.id, title "
               "FROM verses INNER JOIN books ON (verses.book=books.id)");

    while (query.next()) {
        this->addVerse(Verse(query.value(0).toInt(),
                             query.value(1).toInt(),
                             query.value(2).toInt(),
                             query.value(3).toString(),
                             Book(query.value(4).toInt(),
                                  query.value(5).toString()
                                  )
                             )
                       );
    }
}

int VersesModel::rowCount(const QModelIndex & /* parent */) const {
    return m_verses.count();
}

void VersesModel::addVerse(const Verse &verse)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_verses << verse;
    endInsertRows();
}

QVariant VersesModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_verses.count())
        return QVariant();

    const Verse &verse = m_verses[index.row()];
    if (role == IdRole)
        return verse.id();
    else if (role == ChapterRole)
        return verse.chapter();
    else if (role == VerseRole)
        return verse.verse();
    else if (role == ContentRole)
        return verse.content();
    else if (role == SectionRole)
        return verse.section();
    else if (role == BookRole)
        return verse.book_id();
    return QVariant();
}

QHash<int, QByteArray> VersesModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[ChapterRole] = "chapter";
    roles[VerseRole] = "verse";
    roles[ContentRole] = "content";
    roles[SectionRole] = "section";
    roles[BookRole] = "book_id";
    return roles;
}

FilterProxyModel::FilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
    setSortOrder(false);
}

FilterProxyModel::~FilterProxyModel()
{

}

void FilterProxyModel::setFilterString(QString string)
{
    this->setFilterCaseSensitivity(Qt::CaseInsensitive);
    this->setFilterFixedString(string);
}

void FilterProxyModel::setSortOrder(bool checked)
{
    if(checked)
    {
        this->sort(0, Qt::DescendingOrder);
    }
    else
    {
        this->sort(0, Qt::AscendingOrder);
    }
}

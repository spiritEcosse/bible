#include <QString>
#include <QSqlDatabase>

#ifndef DBMANAGER_H
#define DBMANAGER_H


class DbManager
{
public:
    static DbManager* getInstance();
    QSqlDatabase db;
private:
    DbManager();
    DbManager(const DbManager& );
    DbManager& operator=(const DbManager& );
};

#endif // DBMANAGER_H

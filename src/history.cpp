#include <QSqlQuery>
#include "history.h"

History::History(
        const QDateTime &dateTime,
        const quint16 &indexUp,
        const quint16 &indexDown
        )
    : m_dateTime(dateTime), m_indexUp(indexUp), m_indexDown(indexDown)
{
}

quint64 History::id() const
{
    return m_id;
}

QString History::section() const
{
    QDateTime now = QDateTime::currentDateTime();
    QString text;

    qint64 days = dateTime().daysTo(now);

    switch (days) {
    case 0:
        text = "Today";
        break;
    case 1:
        text = "Yesterday";
        break;
    default:
        text = days < 7 ? dateTime().toString("dddd") : "Older";
        break;
    }

    return text;
}

QDateTime History::dateTime() const
{
    return m_dateTime;
}

quint16 History::indexUp() const
{
    return m_indexUp;
}

quint16 History::indexDown() const
{
    return m_indexDown;
}

HistoryModel::HistoryModel(QObject *parent) : QAbstractListModel(parent)
{
    QSqlQuery query(UserDB::getInstance()->db);
    query.exec("SELECT id, dateTime, indexUp, indexDown "
               "FROM history ORDER BY ID DESC");

    while (query.next()) {
        this->addHistory(
                    History(
                        query.value(1).toDateTime(),
                        query.value(2).toInt(),
                        query.value(3).toInt()
                        )
                    );
    }
}


int HistoryModel::rowCount(const QModelIndex & /* parent */) const{
    return addedRows;
}

void HistoryModel::addHistory(const History &history)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_stories << history;
    endInsertRows();
}

QVariant HistoryModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_stories.count())
        return QVariant();

    const History &history = m_stories[index.row()];
    if (role == IdRole)
        return history.id();
    else if (role == DateTimeRole)
        return history.dateTime();
    else if (role == IndexDown)
        return history.indexDown();
    else if (role == IndexUp)
        return history.indexUp();
    else if (role == SectionRole)
        return history.section();
    return QVariant();
}


QHash<int, QByteArray> HistoryModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[DateTimeRole] = "dateTime";
    roles[IndexDown] = "indexDown";
    roles[IndexUp] = "indexUp";
    roles[SectionRole] = "section";
    return roles;
}

void HistoryModel::add(const QDateTime &dateTime, const quint16 &indexUp, const quint16 &indexDown)
{
    QSqlQuery query(UserDB::getInstance()->db);
    query.prepare("INSERT INTO history(dateTime, indexUp, indexDown) values(:dateTime, :indexUp, :indexDown)");
    query.bindValue(":dateTime", dateTime);
    query.bindValue(":indexUp", indexUp);
    query.bindValue(":indexDown", indexDown);
    query.exec();

    beginInsertRows(QModelIndex(), 0, 0);
    m_stories.prepend(History(dateTime, indexUp, indexDown));
    addedRows++;
    endInsertRows();
    emit numberPopulated(1);
}

void HistoryModel::fetchMore(const QModelIndex &) {
    int remainder = m_stories.count() - addedRows;
    int itemsToFetch = qMin(15, remainder);

    if (itemsToFetch <= 0)
        return;

    beginInsertRows(QModelIndex(), addedRows, addedRows + itemsToFetch - 1);
    addedRows += itemsToFetch;
    endInsertRows();
    emit numberPopulated(itemsToFetch);
}

bool HistoryModel::canFetchMore(const QModelIndex & /* index */) const
{
    return addedRows < m_stories.count() ? true : false;
}

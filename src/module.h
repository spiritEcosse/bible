#ifndef MODULE_H
#define MODULE_H

#include <QSortFilterProxyModel>
#include <QDate>
#include <QJsonValue>
#include <QLocale>

class Module
{
public:
    Module(
            const QString &name,
            const QString &description,
            const QString &abbreviation,
            const QString &information,
            const QLocale &language,
            const QString &language_load,
            const QDate &update,
            const QList<QString> &urls,
            const QList<QHash<QString, QString>> &locale_desc,
            const QString &comment,
            const quint32 &size,
            const QString &region,
            const bool &def,
            const bool &hidden,
            const QString &copyright
    );

    QString name() const;
    QString description() const;
    QString abbreviation() const;
    QString information() const;
    QLocale language() const;
    QString language_load() const;
    QDate update() const;
    QList<QString> urls() const;
    QList<QHash<QString, QString>> locale_desc() const;
    QString comment() const;
    quint32 size() const;
    QString region() const;
    bool def() const;
    bool hidden() const;
    QString copyright() const;
    QString section() const;
private:
    QString m_name;
    QString m_description;
    QString m_abbreviation;
    QString m_information;
    QLocale m_language;
    QString m_language_load;
    QDate m_update;
    QList<QString> m_urls;
    QList<QHash<QString, QString>> m_locale_desc;
    QString m_comment;
    quint32 m_size;
    QString m_region;
    bool m_def;
    bool m_hidden;
    QString m_copyright;
};

class ModuleModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum ModuleModelRoles {
        UpdateRole,
        DescriptionRole,
        AbbreviationRole,
        InformationRole,
        CommentRole,
        SizeRole,
        LanguageRole,
        SectionRole
    };

    ModuleModel(QObject *parent = 0);
    ~ModuleModel();
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    Module parserJson(const QJsonValue &jsonValue) const;
    quint32 correctSize(const QJsonValue &jsonValue) const;
    QLocale correctLanguage(const QJsonObject &jsonObject) const;
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<Module> m_modules;
    int addedRows = 0;
signals:
    void numberPopulated(int number);
};


class ModuleProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    ModuleProxyModel(QObject* parent = 0);

    ~ModuleProxyModel();

};


#endif // MODULES_H

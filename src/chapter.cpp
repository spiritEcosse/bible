#include <QSqlQuery>
#include "chapter.h"
#include "dbmanager.h"

Chapter::Chapter(
        const quint16 &verse_id,
        const quint8 &chapter,
        const Book &book
        )
  : m_verse_id(verse_id), m_chapter(chapter), m_book(book), m_visible(false)
{

}

quint16 Chapter::verse_id() const
{
    return m_verse_id;
}

quint8 Chapter::chapter() const
{
    return m_chapter;
}

quint8 Chapter::book_id() const
{
    return book().id();
}

bool Chapter::visible() const
{
    return m_visible;
}

QString Chapter::section() const
{
    return book().title();
}

quint16 Chapter::book_chapters() const
{
    return book().chapters();
}

Book Chapter::book() const
{
    return m_book;
}

ChaptersModel::ChaptersModel(QObject *parent) : QAbstractListModel(parent)
{
    QSqlQuery query(DbManager::getInstance()->db);
//    query.exec("select books.id, books.title, verses.chapter from books inner join "
//               "verses on (verses.book=books.id) group by books.id");

    query.exec("select books.id, books.title, verses.id, verses.chapter, "
               "(select max(verses.chapter) from verses where verses.book == books.id) as chapters "
               "from verses inner join books on (books.id=verses.book) "
               "where verses.verse=1 "
               "group by verses.book, verses.chapter");

    while (query.next()) {
        this->addChapter(
                    Chapter(
                        query.value(2).toInt(),
                        query.value(3).toInt(),
                        Book(
                            query.value(0).toInt(),
                            query.value(1).toString(),
                            query.value(4).toInt()
                            )
                        )
                    );
    }
}

void ChaptersModel::addChapter(const Chapter &chapter)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_chapters << chapter;
    endInsertRows();
}

QVariant ChaptersModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_chapters.count())
        return QVariant();

    const Chapter &chapter = m_chapters[index.row()];
    if (role == VerseIdRole)
        return chapter.verse_id();
    else if (role == ChapterRole)
        return chapter.chapter();
    else if (role == SectionRole)
        return chapter.section();
    else if (role == VisibleRole)
        return chapter.visible();
    else if (role == BookChaptersRole)
        return chapter.book_chapters();
    else if (role == BookIdRole)
        return chapter.book_id();
    return QVariant();
}

int ChaptersModel::rowCount(const QModelIndex & /* parent */) const {
    return m_chapters.count();
}

QHash<int, QByteArray> ChaptersModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[VerseIdRole] = "verse_id";
    roles[ChapterRole] = "chapter";
    roles[SectionRole] = "section";
    roles[VisibleRole] = "visible";
    roles[BookChaptersRole] = "book_chapters";
    roles[BookIdRole] = "book_id";
    return roles;
}

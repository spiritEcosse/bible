#ifndef BOOKSMODEL_H
#define BOOKSMODEL_H

#include <QObject>

class Book
{
public:
    Book(const quint8 &id, const QString &title, const quint16 &chapters = 0);

    quint8 id() const;
    QString title() const;
    quint16 chapters() const;
private:
    quint8 m_id;
    QString m_title;
    quint16 m_chapters;
};

#endif // BOOKSMODEL_H

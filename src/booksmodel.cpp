#include "booksmodel.h"

Book::Book(const quint8 &id, const QString &title, const quint16 &chapters)
    : m_id(id), m_title(title), m_chapters(chapters)
{
}

quint8 Book::id() const
{
    return m_id;
}

QString Book::title() const
{
    return m_title;
}

quint16 Book::chapters() const
{
    return m_chapters;
}

#ifndef HISTORY_H
#define HISTORY_H

#include <QAbstractListModel>
#include <QDateTime>
#include <QSqlQuery>
#include "userdb.h"

class History
{
public:
    History(
            const QDateTime &dateTime,
            const quint16 &indexUp,
            const quint16 &indexDown
    );

    quint64 id() const;
    QDateTime dateTime() const;
    quint16 indexUp() const;
    quint16 indexDown() const;
    QString section() const;
private:
    quint64 m_id;
    QDateTime m_dateTime;
    quint16 m_indexUp;
    quint16 m_indexDown;
};


class HistoryModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum HistoryRoles {
        IdRole,
        DateTimeRole,
        IndexUp,
        IndexDown,
        SectionRole
    };

    HistoryModel(QObject *parent = 0);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    void addHistory(const History &history);
    Q_INVOKABLE void add(const QDateTime &dateTime, const quint16 &indexUp, const quint16 &IndexDown);
protected:
    bool canFetchMore(const QModelIndex &parent) const override;
    void fetchMore(const QModelIndex &parent) override;

    QHash<int, QByteArray> roleNames() const;
//    QSqlQuery *query;
private:
    QList<History> m_stories;
    int addedRows = 0;
signals:
    void numberPopulated(int number);
};

#endif // HISTORY_H

#ifndef TESTCHAPTER_H
#define TESTCHAPTER_H

#include <QObject>

class TestChapter : public QObject
{
    Q_OBJECT
public slots:
    void toUpper();
};


#endif // TESTCHAPTER_H

# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = bible

CONFIG += sailfishapp
QT += sql \
    core
TEMPLATE = app

DISTFILES += qml/bible.qml \
    qml/cover/CoverPage.qml \
    qml/pages/Books.qml \
    qml/pages/Chapters.qml \
    qml/pages/FlippingPageHeader.qml \
    qml/pages/History.qml \
    rpm/bible.changes.in \
    rpm/bible.changes.run.in \
    rpm/bible.spec \
    rpm/bible.yaml \
    translations/*.ts \
    db/*.bz2 \
    bible.desktop \
    qml/pages/PageEdgeTransition.qml \
    qml/pages/CoverLoader.js \
    qml/pages/private/CoverWindow.qml \
    qml/pages/private/DefaultCover.qml \
    qml/pages/private/LayoutGrid.qml \
    qml/pages/private/PageStackGlassIndicator.qml \
    qml/pages/private/PageStackIndicator.qml \
    qml/pages/private/ReturnToHomeHintCounter.qml \
    qml/pages/Pages.qml \
    qml/pages/Modules.qml \
    registry/registry.json \
    qml/pages/ModuleInformation.qml

OTHER_FILES += db/*.bz2

db.files = db
db.path = /usr/share/$${TARGET}
db.commands = bunzip2 -kf $$PWD/db/*.bz2 && \
    mv $$PWD/db/*.bz2 $$PWD
INSTALLS += db

clear_db.files = db/db/
clear_db.path = /usr/share/$${TARGET}
clear_db.commands = rm -f $$PWD/db/*.db && \
    mv $$PWD/*.bz2 $$PWD/db/
INSTALLS += clear_db

registry.files = registry
registry.path = /usr/share/$${TARGET}
INSTALLS += registry

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
#    sailfishapp_i18n_idbased \

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/bible-ru.ts

#/usr/lib/qt5/qml/Sailfish/Silica/

SOURCES += src/dbmanager.cpp \
    src/bible.cpp \
    src/booksmodel.cpp \
    src/versesmodel.cpp \
    src/userdb.cpp \
    src/history.cpp \
    src/chapter.cpp \
    src/module.cpp \
    src/host.cpp \
    src/registry.cpp

HEADERS += \
    src/dbmanager.h \
    src/booksmodel.h \
    src/versesmodel.h \
    src/userdb.h \
    src/history.h \
    src/chapter.h \
    src/module.h \
    src/host.h \
    src/registry.h

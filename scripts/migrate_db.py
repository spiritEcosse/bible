#!/usr/bin/python
"""Migrate from https: // github.com / liudongmiao / bibledata."""

import sqlite3
import sys


def migrate_db(from_db, to_db):
    """run: ./migrate.py from.db to.db.

    Args:
        from_db (TYPE): source db sqlite
        to_db (TYPE): target db sqlite
    """
    to_db = '{}{}'.format(to_db, '.db')
    from_db_conn = sqlite3.connect(from_db)
    from_db_conn.cursor()
    from_db_conn.row_factory = sqlite3.Row

    to_db_conn = sqlite3.connect(to_db)
    to_db_conn.cursor()
    to_db_conn.row_factory = sqlite3.Row
    to_db_conn.executescript("""CREATE TABLE IF NOT EXISTS `books` (
                `id`            INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `title`         VARCHAR NOT NULL
        );
        CREATE TABLE IF NOT EXISTS `verses` (
                `id`        INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                `book`      INTEGER NOT NULL,
                `chapter`   INTEGER NOT NULL,
                `verse`     INTEGER NOT NULL,
                `content`   TEXT NOT NULL
        );
        CREATE INDEX IF NOT EXISTS `verses_book` ON `verses` ( `book`  ASC);
        CREATE INDEX IF NOT EXISTS `verses_book_chapter` ON `verses` (
                `book`      ASC,
                `chapter`   ASC
        );
        CREATE UNIQUE INDEX IF NOT EXISTS `verses_book_chapter_verse` ON `verses` (
                `book`      ASC,
                `chapter`   ASC,
                `verse`     ASC
    );""")

    for row in from_db_conn.execute("SELECT * FROM books order by number"):
        with to_db_conn:
            to_db_conn.execute(
                "INSERT INTO books (title) VALUES(?)",
                (row['human'],))

    chapter_prev = None
    book_prev = None

    for row in from_db_conn.execute(
            "SELECT * FROM verses INNER JOIN books ON (verses.book == books.osis) order by verses.id"
    ):
        with to_db_conn:
            chapter = str(row['verse']).split('.')[0]
            book = row['book']

            if chapter == chapter_prev and book == book_prev:
                verse += 1
            else:
                verse = 1

            to_db_conn.execute(
                "INSERT INTO verses (book, chapter, verse, content) VALUES(?, ?, ?, ?)",
                (row['number'], chapter, verse, row['unformatted']))
            chapter_prev = chapter
            book_prev = book


if __name__ == '__main__':
    migrate_db(*sys.argv[1:])

import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow
{
    initialPage: chapters
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: Orientation.All
    _defaultPageOrientations: Orientation.All
    property Component chapters: Component { Chapters {} }
    property Component history: Component { History {} }
    property Component modules: Component { Modules {} }
    property Component moduleInformation: Component { ModuleInformation {} }
    property int operationType: PageStackAction.Animated
}

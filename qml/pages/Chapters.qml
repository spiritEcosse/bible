import QtQuick 2.0
import Sailfish.Silica 1.0
import QtFeedback 5.0

Pages {
    id: page
    property int currentIndex: 0
    property bool searchMode
    property string searchText: searchField.text
    property Component pageRight: history

    onStatusChanged: {
        if (status == PageStatus.Active) {
            pageStack.pushAttached(pageRight)
        }
    }

    ThemeEffect {
        id: longBuzz
        effect: ThemeEffect.PressStrong
    }

    Drawer {
        id: drawerBooks
        dock: Dock.Left
        anchors.fill: parent
        background: SilicaListView {
            id: listBooks
            model: chaptersListModel
            anchors.fill: parent
            property int bookId
            onBookIdChanged: {
                for (var QModelIndex, pos = 0; pos < model.rowCount(); pos++ ) {
                    QModelIndex = model.index(pos, 0)

                    if ( model.data(QModelIndex, 5) === bookId ) {
                        currentIndex = pos
                        return
                    }
                }
            }
            snapMode: ListView.SnapToItem
            highlightRangeMode: ListView.StrictlyEnforceRange
            highlightFollowsCurrentItem: true

            section {
                property: "section"
                criteria: ViewSection.FullString
                delegate: Item {
                    id: sectionHeaderRect
                    width: parent.width
                    height: Theme.itemSizeSmall

                    Rectangle {
                        anchors.fill: parent
                        color: Theme.primaryColor
                        opacity: 0.15
                    }

                    Label {
                        text: section
                        color: Theme.highlightColor
                        font.pixelSize: Theme.fontSizeMedium
                        wrapMode: Text.WordWrap
                        truncationMode: TruncationMode.Fade
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }

            delegate: ListItem {
                contentHeight: chapter.height
                BackgroundItem {
                    onClicked: {
                        listBooks.currentIndex = index
                        listView.currentIndex = model.verse_id - 1
                    }
                }

                Item {
                    id: chapter
                    width: parent.width
                    height: Theme.itemSizeSmall

                    Rectangle {
                        width: parent.width
                        height: parent.height
                        color: Theme.primaryColor
                        opacity: 0.05
                        visible: index + 1 & 1
                    }

                    Label {
                        text: model.chapter
                        anchors.centerIn: parent
                    }
                }
            }
        }

        SilicaFlickable {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width

            PushUpMenu {
                id: pushUpMenu
                MenuItem {
                    text: qsTrId("Search")
                    onDelayedClick: searchMode = true
                }
                MenuItem {
                    text: listView.navigateByBook === false ? qsTrId("Slip by books") : qsTrId("Slip by chapters")
                    onDelayedClick: listView.navigateByBook = !listView.navigateByBook
                }
                MenuItem {
                    text: qsTrId("Books")
                    onDelayedClick: drawerBooks.open = true
                }
                MenuItem {
                    text: qsTrId("Modules")
                    onDelayedClick: pageStack.push(modules, {}, operationType)
                }
            }

            FlippingPageHeader {
                id: header
                animate: page.status === PageStatus.Active
                width: parent.width
                title: listView.currentSection
            }

            Image {
                width: parent.width
                height: header.height
                source: "image://theme/graphic-gradient-edge"
                rotation: 180
            }

            SilicaListView {
                id: listView
                objectName: "Chapters"
                width: parent.width
                anchors {
                    left: parent.left
                    right: parent.right
                    top: header.bottom
                    bottom: bottomImg.top
                }
                clip: true
                property bool navigateByBook: false
                property int bottomY: 0
                property int topY: 0
                property int heightPage: Screen.height
                property int nextPage: contentY + heightPage - header.height - bottomImg.height - searchField.height - bottomY
                property int prevPage: contentY + topY
                property var verseIdDown: indexAt(0, nextPage)
                property int actionArea: Theme.horizontalPageMargin
                currentIndex: page.currentIndex
                model: filterVerses
                spacing: Theme.paddingMedium
                snapMode: ListView.SnapToItem
                highlightRangeMode: ListView.StrictlyEnforceRange
                onMovementEnded: listView.history()
                Component.onCompleted: listView.history()
                onContentHeightChanged: listView.history()
                highlightFollowsCurrentItem: true
                highlightMoveDuration: 0
                VerticalScrollDecorator {}

                MouseArea {
                    enabled: drawerBooks.open
                    anchors.fill: parent
                    onClicked: drawerBooks.open = false
                }

                MouseArea {
                    enabled: slideListView.running
                    anchors.fill: parent
                    onClicked: slideListView.stop()
                }

                section {
                    property: 'section'
                    delegate: Item {
                        height: Theme.itemSizeSmall
                        width: parent.width

                        Rectangle {
                            anchors.fill: parent
                            color: Theme.primaryColor
                            opacity: 0.15
                        }

                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            color: Theme.highlightColor
                            text: section
                            font.pixelSize: Theme.fontSizeMedium
                        }
                    }
                }
                delegate: ListItem {
                    id: listItem
                    menu: contextMenu
                    focus: true
                    contentHeight: child.height
                    property var verse: model
                    width: parent.width

                    Item {
                        id: child
                        height: childrenRect.height
                        x: Theme.paddingMedium
                        width: listView.width - 2*Theme.paddingMedium

                        Label {
                            color: Theme.highlightColor
                            id: indexModel
                            width: listView.actionArea
                            text: model.verse + '.'
                            font.italic: true
                            font.pixelSize: Theme.fontSizeExtraSmall
                            horizontalAlignment: Text.AlignRight
                            anchors {
                                left: parent.left
                            }
                        }
                        Label {
                            wrapMode: Text.WordWrap
                            truncationMode: TruncationMode.Fade
                            text: {
                                content ? Theme.highlightText(
                                              content.substr(0, content.length),
                                              searchText,
                                              Theme.highlightColor) : "";
                            }
                            font.pixelSize: Theme.fontSizeExtraSmall
                            width: parent.width
                            anchors {
                                leftMargin: Theme.paddingMedium
                                left: indexModel.right
                                right: parent.right
                            }
                        }
                    }

                    ContextMenu {
                        id: contextMenu

                        MenuItem {
                            visible: searchField.needle && searchField.enabled
                            onDownChanged: longBuzz.play()
                            text: qsTrId("Go to")
                            onClicked: {
                                var searchCurrText = searchField.text;
                                searchField.text = '';
                                pageStack.replace(
                                            Qt.resolvedUrl("Chapters.qml"),
                                            {'currentIndex': index, 'searchText': searchCurrText})
                            }
                        }

                        MenuItem {
                            text: qsTr("Copy text");
                            onClicked: Clipboard.text = model.content
                        }
                    }
                }

                function downIndex() {
                    for (bottomY = 0; verseIdDown === -1 && prevPage < nextPage; bottomY += spacing) {}
                }

                function history() {
                    downIndex()
                    timerStoriesAdd.restart()

                    var QModelIndex = model.index(currentIndex !== -1 ? currentIndex : 0, 0)
                    listBooks.bookId = model.data(QModelIndex, 1)
                }

                function verticalSlip(direction) {
                    timerVerticalSlip.direction = direction

                    if (timerVerticalSlip.running) {
                        timerVerticalSlip.stop()
                        doubleClick(direction)
                    } else {
                        timerVerticalSlip.start()
                    }
                }

                Timer {
                    id: timerStoriesAdd
                    interval: 2000
                    repeat: false
                    onTriggered: {
                        if (listView.verseIdDown !== -1 && listView.currentIndex) {
                            stories.add(new Date(), listView.currentIndex, listView.verseIdDown)
                        }
                    }
                }

                Timer {
                    id: slideListView
                    interval: 100
                    repeat: true
                    property MouseArea direction
                    onTriggered: listView.doubleClick(direction)
                    triggeredOnStart: true
                }

                Timer {
                    id: timerVerticalSlip
                    interval: 200
                    property MouseArea direction
                    onTriggered: listView.singleClick(direction)
                }

                function singleClick(direction) {
                    switch (direction) {
                    case up:
                        listView.decrementCurrentIndex()
                        break
                    case down:
                        listView.currentIndex = listView.verseIdDown
                        break
                    }

                    listView.history()
                }
                function doubleClick(direction) {
                    if (listView.navigateByBook === true) {
                        listView.toBook(direction)
                    } else {
                        listView.toChapter(direction)
                    }
                }
                function toChapter(direction) {
                    var chaptersCurrentIndex = direction.chaptersCurrentIndex

                    if ( chaptersCurrentIndex < chaptersListModel.rowCount() && chaptersCurrentIndex >= 0) {
                        listBooks.currentIndex = chaptersCurrentIndex
                        var QModelIndex = chaptersListModel.index(chaptersCurrentIndex, 0)
                        listView.currentIndex = chaptersListModel.data(QModelIndex, 0) - 1
                        listView.history()
                    }
                }
                function toBook(direction) {
                    var chapterIndex = listBooks.currentIndex
                    var chaptersBook, QModelIndex, chapterCurrent

                    switch (direction) {
                    case up:
                        QModelIndex = chaptersListModel.index(chapterIndex, 0)
                        chapterCurrent = chaptersListModel.data(QModelIndex, 1)
                        chapterIndex += -chapterCurrent

                        if (chapterIndex >= 0) {
                            QModelIndex = chaptersListModel.index(chapterIndex, 0)
                            chapterIndex += -chaptersListModel.data(QModelIndex, 4) + 1
                        }
                        break
                    case down:
                        QModelIndex = chaptersListModel.index(chapterIndex, 0)
                        chaptersBook = chaptersListModel.data(QModelIndex, 4)
                        chapterCurrent = chaptersListModel.data(QModelIndex, 1)
                        chapterIndex += -chapterCurrent + chaptersBook + 1
                        break
                    }

                    if (chapterIndex < chaptersListModel.rowCount() && chapterIndex >= 0) {
                        listBooks.currentIndex = chapterIndex
                        QModelIndex = chaptersListModel.index(chapterIndex, 0)
                        listView.currentIndex = chaptersListModel.data(QModelIndex, 0) - 1
                        listView.history()
                    }
                }

                NumberAnimation {
                    id: naListView
                    target: listView
                    property: "contentY"
                }

                MouseArea {
                    id: up
                    enabled: !drawerBooks.open
                    width: Theme.itemSizeMedium
                    height: Screen.height / 2 - listView.anchors.topMargin
                    anchors.top: parent.top
                    anchors.left: parent.left
                    onClicked: listView.verticalSlip(up)
                    property int chaptersCurrentIndex: listBooks.currentIndex - 1
                    property int index: listView.currentIndex
                    property real positionMode: ListView.End
                    onPressAndHold: {
                        slideListView.direction = up
                        slideListView.restart()
                    }
                }

                MouseArea {
                    id: down
                    width: up.width
                    enabled: up.enabled
                    anchors.top: up.bottom
                    anchors.bottom: parent.bottom
                    anchors.left: up.left
                    onClicked: listView.verticalSlip(down)
                    property int chaptersCurrentIndex: listBooks.currentIndex + 1
                    property int index: listView.verseIdDown
                    property real positionMode: ListView.Beginning
                    onPressAndHold: {
                        slideListView.direction = down
                        slideListView.restart()
                    }
                }

                MouseArea {
                    id: upRight
                    width: up.width
                    enabled: up.enabled
                    height: up.height
                    anchors.top: parent.top
                    anchors.right: parent.right
                    onClicked: listView.verticalSlip(up)
                    onPressAndHold: {
                        slideListView.direction = up
                        slideListView.restart()
                    }
                }

                MouseArea {
                    width: upRight.width
                    enabled: down.enabled
                    anchors.top: upRight.bottom
                    anchors.bottom: parent.bottom
                    anchors.right: upRight.right
                    onClicked: listView.verticalSlip(down)
                    onPressAndHold: {
                        slideListView.direction = down
                        slideListView.restart()
                    }
                }
            }

            Image {
                id: bottomImg
                width: parent.width
                property int defHeight: searchField.enabled ? 0 : Theme.itemSizeExtraSmall
                height: defHeight + searchField.height
                source: "image://theme/graphic-gradient-edge"
                anchors.bottom: parent.bottom
            }

            SearchField {
                id: searchField
                property bool active: activeFocus || text.length > 0
                property bool needle: text.length > 2
                onActiveChanged: if (!active) searchMode = false
                onEnabledChanged: {
                    if (enabled) {
                        text = ""
                        forceActiveFocus()
                    }
                }
                anchors.bottom: parent.bottom
                onTextChanged: {
                    searchText = text
                    if (text.length > 2 || text.length === 0) {
                        filterVerses.setFilterString(text)
                    }
                }
                enabled: searchMode
                width: parent.width
                height: enabled ? implicitHeight : 0.0
                opacity: enabled ? 1.0 : 0.0
                Behavior on opacity { FadeAnimator {} }
                Behavior on height { NumberAnimation { duration: 200; easing.type: Easing.InOutQuad } }
                EnterKey.iconSource: "image://theme/icon-m-enter-close"
                EnterKey.onClicked: focus = false
            }
        }
    }
}

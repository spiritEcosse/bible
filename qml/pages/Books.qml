import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    property Component pageLeft: history
    property Component pageRight: chapters

    SilicaListView {
        id: listView
        anchors.fill: parent
        model: chaptersListModel
        property int setCurrentIndex
        property string expandedSection: ""

        header: PageHeader {
            id: header
            title: qsTrId("Books")
        }
        section {
            property: "section"
            criteria: ViewSection.FullString
            delegate: Item {
                id: sectionHeaderRect
                width: parent.width
                height: Theme.itemSizeSmall
                property bool isExpanded: false
                property string currentExpandedSection: listView.expandedSection

                Rectangle {
                    anchors.fill: parent
                    color: Theme.primaryColor
                    opacity: 0.15
                }

                Label {
                    text: section
                    color: Theme.highlightColor
                    font.pixelSize: Theme.fontSizeMedium
                    anchors.centerIn: parent
                }

                onCurrentExpandedSectionChanged: {
                    if(currentExpandedSection === section)
                        isExpanded = true;
                    else
                        isExpanded = false;
                }

                onIsExpandedChanged: {
                    if (isExpanded) {
                        listView.expandedSection = section;
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        sectionHeaderRect.isExpanded = !sectionHeaderRect.isExpanded;
                    }
                }
            }
        }

        Timer {
            id: timerUpdateContent
            interval: 60
            repeat: false
            triggeredOnStart: false
            onTriggered: listView.positionViewAtIndex(listView.setCurrentIndex, ListView.Beginning)
        }

        delegate: ListItem {
            id: listItem
            contentHeight: child.visible ? child.height : 0
            BackgroundItem {
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("Chapters.qml"), {'currentIndex': model.verse_id - 1})
                }
            }

            Item {
                id: child
                visible: model.section === listView.expandedSection
                height: visible ? Theme.itemSizeSmall : 0
                width: parent.width
                onVisibleChanged: {
                    if (model.chapter === 1 && child.visible) {
                        listView.setCurrentIndex = index
                        timerUpdateContent.restart()
                    }
                }

                Rectangle {
                    width: parent.width
                    height: parent.height
                    color: Theme.primaryColor
                    opacity: 0.05
                    visible: index + 1 & 1
                }

                Label {
                    text: model.chapter
                    anchors.centerIn: parent
                }
            }
        }
    }
}

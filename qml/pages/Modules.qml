import QtQuick 2.0
import Sailfish.Silica 1.0

Pages {
    id: page

    SilicaFlickable {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: parent.width

        FlippingPageHeader {
            id: header
            animate: page.status === PageStatus.Active
            width: parent.width
            title: listView.currentSection
        }

        Image {
            width: parent.width
            height: header.height
            source: "image://theme/graphic-gradient-edge"
            rotation: 180
        }

        SilicaListView {
            id: listView
            clip: true
            anchors {
                left: parent.left
                right: parent.right
                top: header.bottom
                bottom: parent.bottom
            }
            spacing: Theme.paddingLarge
            snapMode: ListView.SnapToItem
            highlightRangeMode: ListView.StrictlyEnforceRange

            header: PageHeader {
                title: qsTrId("Modules")
            }

            model: moduleProxyModel

            section {
                property: 'section'

                delegate: SectionHeader {
                    text: section
                    height: Theme.itemSizeExtraSmall
                }
            }

            VerticalScrollDecorator {}

            delegate: BackgroundItem {
                id: listItem
                menu: contextMenu
                contentHeight: child.height
                onClicked: remorse.execute(
                               listItem,
                               qsTrId("deleting"),
                               function() { console.log("deleting") }
                               )


                RemorsePopup { id: remorse }

                Item {
                    id: child
                    x: Theme.paddingMedium
                    height: childrenRect.height
                    width: parent.width - 2 * x

                    Label {
                        id: desc
                        wrapMode: Text.WordWrap
                        truncationMode: TruncationMode.Fade
                        text: description + ' (' + abbreviation + ')'
                        font.pixelSize: Theme.fontSizeMedium
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                    }
                    Label {
                        id: dateUpdate
                        horizontalAlignment: Text.AlignLeft
                        font.pixelSize: Theme.fontSizeExtraSmall
                        text: qsTrId("Last module update date: ") + Qt.formatDate(updateDate, 'dd.MM.yyyy')
                        anchors {
                            top: desc.bottom
                            topMargin: Theme.paddingSmall
                            left: parent.left
                        }
                    }
                    Label {
                        text: "- " + Format.formatFileSize(size, 2)
                        font.pixelSize: Theme.fontSizeExtraSmall * 3 / 4
                        font.italic: true
                        anchors {
                            top: desc.bottom
                            topMargin: Theme.paddingSmall
                            left: dateUpdate.right
                            right: parent.right
                        }
                    }
                }

                ContextMenu {
                    id: contextMenu

                    MenuItem {
                        text: qsTrId("Information")
                        onClicked: pageStack.push(
                                       moduleInformation,
                                       {
                                           'information': information,
                                           'comment': comment
                                       })
                    }
                }
            }
        }
    }
}
